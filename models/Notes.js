var mongoose = require('mongoose');

var NoteSchema = new mongoose.Schema({
  header: String,
  content: String,
  posX: Number,
  posY: Number
})

mongoose.model('Note', NoteSchema);