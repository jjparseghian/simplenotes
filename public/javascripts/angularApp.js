// ;(function(){

  var app = angular.module("notesApp", ['ui.router', ]);

  app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
    $stateProvider.state('home', {
      url: '/home',
      controller: 'MainController',
      resolve: {
        postPromise: ['posts', function(posts){
          return posts.getAll();
        }]
      }
    })

    $urlRouterProvider.otherwise('home');
  }])

  app.controller('MainController', ['$scope', '$http', 'posts', function($scope, $http, posts){


    var Note = function(header, content, posX, posY){
      // this.id = cuniq;
      this.header = header;
      this.content = content;
      this.posX = posX;
      this.posY = posY;
    }

    var socket = io.connect("http://192.168.1.108:3000");
    $scope.socket = socket;

    socket.on('updates', function(data){
      console.log('updating dom ele', data)
      // var el = document.getElementById(data._id);
      // el.style.top = data.posY;
      // el.style.left = data.posX;
      // console.log("el", el)
      $("#" + data._id).css({
        top: data.posY,
        left: data.posX
      })

      $('#' + data._id + " " + ".header").html(data.header)
      $('#' + data._id + " " + ".content").html(data.content)
    })

    socket.on('newNotes', function(data){
      // $scope.updateView()
      console.log("newNotes data", data);
    })

    $scope.notes = posts.posts
    $scope.newNote = function(){
      var note = new Note("Enter Header Here", "Enter Content Here", 0, 0)
      posts.create(note)
      $scope.socket.emit('newNoteCreated', note)
    }

    $scope.removeNote = function(noteId){
      posts.deleteNote(noteId)
    }

    $scope.updateNote = function(noteId, header, content, posX, posY){
      posts.update(noteId, header, content, posX, posY);
      var note = {
        noteId: noteId,
        header: header,
        content: content,
        posX: posX,
        posY: posY
      }
      $scope.socket.emit('update', {_id: noteId, header: header, content: content})

    }

    $scope.updateView = function(){
      // $scope.notes = posts.getAll().$$state.value.data
      // console.log("get all ", posts.getAll())
    }
  }]);

  app.directive('notes', function(){
    // Runs during compile
    return {
      // name: '',
      // priority: 1,
      // terminal: true,
      scope: {
        info: '='
      }, // {} = isolate, true = child, false/undefined = no change
      controller: 'MainController',
      // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
      restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
      // template: '',
      templateUrl: '/templates/notes.html',
      // replace: true,
      // transclude: true,
      // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
      link: function($scope, iElm, iAttrs, controller) {
        // console.log("scope", $scope, "iElm", iElm, "iattrs", iAttrs, "controller", controller)

        function setPos(x, y){
          // console.log( " x ", x, " y ", y)
          $scope.info.posX = x;
          $scope.info.posY = y;
          console.log("scope", $scope)
        }

        iElm.children().click(function(){
          $(this).draggable({disabled: false,
            drag: function(event, ui){
              $scope.$apply(setPos(ui.position.left, ui.position.top));
              $scope.socket.emit('update', $scope.info);
            },
            stop: function(event, ui){
              $scope.updateNote($scope.info._id, $scope.info.header, $scope.info.content, $scope.info.posX, $scope.info.posY)
            }
          })
        })
        .dblclick(function(){
          $(this).draggable({disabled: true});
        })
        .css({
          top: $scope.info.posY,
          left: $scope.info.posX
        })
      }
    };
  });

app.directive('contenteditable', function(){
    // Runs during compile
    return {
      // name: '',
      // priority: 1,
      // terminal: true,
      // scope: {}, // {} = isolate, true = child, false/undefined = no change
      // controller: function($scope, $element, $attrs, $transclude) {},
      require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
      // restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
      // template: '',
      // templateUrl: '',
      // replace: true,
      // transclude: true,
      // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
      link: function($scope, iElm, iAttrs, ngModel) {
        function read(){
          ngModel.$setViewValue(iElm.html());
        }

        ngModel.$render = function(){
          iElm.html(ngModel.$viewValue || "");
        }

        iElm.bind("blur keyup change", function(){
          $scope.$apply(read)
          // console.log("a change is being made", "ielm", iElm, 'ngModel', ngModel, "scope", $scope.info._id )
          $scope.updateNote($scope.info._id, $scope.info.header, $scope.info.content, $scope.info.posX, $scope.info.posY);
        })
      }
    };
  });

app.factory('posts', ['$http', function($http){
  var o = {
    posts: []
  };

  o.getAll = function(){
    return $http.get('/notes').success(function(data){
      angular.copy(data, o.posts);
    })
  }

  o.newNotes = function(note){
    o.posts.push(note)
  }

  o.create = function(note){
    return $http.post('/notes', note).success(function(data){
      o.posts.push(data)
    });
  }

  o.deleteNote = function(noteId){
    return $http.delete('/notes/' + noteId).success(function(data){
      console.log("data", data)
      for (var i = 0; i < o.posts.length; i++) {
        if (o.posts[i]._id === noteId) {
          o.posts.splice(i, 1);
          break;
        }
      };
    }).error(function(err){
      console.log("something went wrong", err)
    })
  }

  o.update = function(noteId, header, content, posX, posY){
    var updatedNote = {
      header: header,
      content: content,
      posX: posX,
      posY: posY
    }

    return $http.put('/notes/' + noteId, updatedNote).success(function(data){
      console.log('updated note', data);
    })
  }

  return o;
}])



// }());