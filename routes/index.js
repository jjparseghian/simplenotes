var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

var mongoose = require('mongoose');
require('../models/Notes');
var Note = mongoose.model('Note');

router.get('/notes', function(req, res, next){
  Note.find(function(err, notes){
    if (err) { return next(err); };

    res.json(notes);
  })
})

router.post('/notes', function(req, res, next){
  var note = new Note(req.body);

  note.save(function(err, note){
    if (err) { return next(err); };

    res.json(note)
  })
})

router.param('note', function(req, res, next, id){
  var query = Note.findById(id);

  query.exec(function(err, note){
    if (err) { return next(err); };
    if (!note) { return next(new Error('cannot find note')); };

    req.note = note;
    return next();
  })
})

// route to test query functionality
router.get('/notes/:note', function(req, res){
  res.json(req.note)
})

router.put('/notes/:note', function(req, res, next){

  Note.findById(req.params.note, function (err, note) {
    if (err) { return next(err); };

    note.header = req.body.header;
    note.content = req.body.content;
    note.posX = req.body.posX;
    note.posY = req.body.posY;

    note.save(function(err){
      if (err) { return next(err); };

      res.json({message: "Successfully Updated"});
    })
    res.json(note);
  });

})

router.delete('/notes/:note', function(req, res){
  Note.remove({
    _id: req.params.note
    // _id: req
  }, function(err, note){
    if (err) {
      // console.log("req", req, "res", res, "note", note, "err", err)
      return res.send(err);
    }

    res.json({message: 'Successfully Deleted'});
  })

  // Note.findById(req.params.id, function(err, note){
  //   note.remove(function(err, note){
  //     res.redirect('/');
  //   })
  // })

})

// router.destroy()

module.exports = router;
